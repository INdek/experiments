#ifdef GL_ES
  precision mediump float;
#endif

attribute vec3 position;
attribute vec3 normal;

uniform mat3 normalMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

varying vec3 v_normal;
varying vec3 v_position;

void main() {
  /*
  v_normal = normalize(normalMatrix * normal);
  vec4 pos = modelViewMatrix * vec4(position, 1.0);
  v_position = pos.xyz;
  gl_Position = projectionMatrix * pos;
  */
  v_normal = normal;
  gl_Position = vec4(position, 1.0);
}
