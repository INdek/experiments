#ifdef GL_ES
  precision mediump float;
#endif

uniform float time;
uniform vec2 resolution;

varying vec3 v_position;
varying vec3 v_normal;

void main() {
  gl_FragColor = vec4(
    exp(v_normal.xy),
    mod(time, 1.0),
    1.0
  );
}
