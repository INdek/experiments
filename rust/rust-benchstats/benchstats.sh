#!/bin/sh
# Copyright (C) Afonso Bordado
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 only,
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# TODO: Fail gracefully and for example return to the correct branch

TRIPLET=$(gcc -dumpmachine)

BENCH_OUTPUT=""

ORIGINAL_COMMIT=$(git rev-parse HEAD)

BENCH_BRANCH=benches
BENCH_OUTPUT_DIR="$TRIPLET/"


# TODO: Cleanup and reset email/name after we use them
check_git_ident () {
	if [ "$(git config user.name)" = "" ] ||
		[ "$(git config user.email)" = "" ]; then

	 	git config user.email "benchstats@az8.co"
  		git config user.name "Benchstats"
	fi
}

# TODO: Remove after we are done
# TODO: todo fetch with a name known to be available (i.e upstream
# could already be taken)
# TODO: Handle more than just Github
# TODO: Handle ssh remote urls
fetch_upstream () {
	REMOTE_URL=$(git config --get remote.origin.url)
	NEW_REMOTE_URL=$(echo $REMOTE_URL | sed -e "s/github.com/$GH_TOKEN@github.com/g")
	git remote add upstream $NEW_REMOTE_URL
	if [ "$?" != "0" ]; then
		echo "Add upstream \"$NEW_REMOTE_URL\" failed"
		exit 1
	fi

	git fetch -q upstream
	if [ "$?" != "0" ]; then
		echo "Fetch upstream failed"
		exit 1
	fi
}

benchmark () {
	BENCH_OUTPUT=$(cargo bench -q $CARGOFLAGS)
	if [ "$?" != "0" ]; then
		echo "cargo bench $CARGOFLAGS failed"
		exit 1
	fi
}

checkout_bench_branch () {
	git checkout -b $BENCH_BRANCH upstream/$BENCH_BRANCH
	if [ "$?" != "0" ]; then
		echo "Reset to $BENCH_BRANCH failed"
		exit 1
	fi
	git status
}

check_output_path () {
	if [ ! -d "$BENCH_OUTPUT_DIR" ]; then
		mkdir -p $BENCH_OUTPUT_DIR
		echo "Created $BENCH_OUTPUT_DIR"
	fi
}

# TODO: Handle more than just one possible branch
save_results () {
	echo "$BENCH_OUTPUT" > $BENCH_OUTPUT_DIR/$ORIGINAL_COMMIT
}
commit () {
	git add $BENCH_OUTPUT_DIR/$ORIGINAL_COMMIT
	if [ "$?" != "0" ]; then
		echo "Failed to add file to staging"
		exit 1
	fi

	git commit -q -m "Automated benchmarking: $TRIPLET"
	if [ "$?" != "0" ]; then
		echo "Commit failed"
		exit 1
	fi
}
push () {
	git push -q upstream $BENCH_BRANCH
	if [ "$?" != "0" ]; then
		echo "Push failed"
		exit 1
	fi
}


benchmark
check_git_ident
fetch_upstream
checkout_bench_branch
check_output_path
save_results
commit
push
