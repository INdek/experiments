# rust-start [![Build Status](https://travis-ci.org/afonso360/rust-start.svg?branch=master)](https://travis-ci.org/afonso360/rust-start) [![Build status](https://ci.appveyor.com/api/projects/status/u1y0y0qiydklqs5r?svg=true)](https://ci.appveyor.com/project/afonso360/rust-start) [![Coverage Status](https://coveralls.io/repos/github/afonso360/rust-start/badge.svg?branch=master)](https://coveralls.io/github/afonso360/rust-start?branch=master)

Start point for rust libraries or binaries that provides appveyor and travis-ci integration
