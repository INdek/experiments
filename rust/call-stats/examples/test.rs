#![feature(proc_macro)]

extern crate call_stats;

use call_stats::record_stats;

#[derive(Debug)]
struct A {
    calls: Vec<usize>,
}

#[record_stats]
impl A {
    fn foo() {}
    fn bar() {}
}


trait HelloWorld {
    fn hello_world();
}

fn main() {
    let a = A { calls: vec![] };
    println!("{:?}", a);
}
