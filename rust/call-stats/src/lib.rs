#![feature(proc_macro)]
extern crate proc_macro;
extern crate syn;

#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate quote;

use proc_macro::TokenStream;
use syn::ImplItem;

error_chain! {}

#[proc_macro_attribute]
pub fn record_stats(args: TokenStream, input: TokenStream) -> TokenStream {
    // Construct a string representation of the type definition
    let s = input.to_string();

    // Parse the string representation
    let ast = syn::parse_item(&s).expect("Failed to parse input");

    // Build the impl
    let new_ast = impl_stats_tracker(ast);

    // Return the generated impl
    gen.parse().expect("Failed to generate output")
}

fn impl_stats_tracker(ast: syn::Item) -> syn::Item {
    use syn::ItemKind::Impl;
    // TODO: Currently assumes we are put directly in an impl
    println!("");
    match ast.node {
        Impl(_, _, _, _, _, mut items) => {
            items = items.into_iter().map(track_fn).collect();
        }
        _ => panic!("Must use #[stats_tracker] on a impl block"),
    };

    ast
}

fn track_fn(item: ImplItem) -> ImplItem {
    item
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}


