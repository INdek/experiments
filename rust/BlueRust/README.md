# BlueRust

To get this going, follow the instructions on the [ble400 repository](https://github.com/japaric/ble400)

and then

```
arm-none-eabi-gdb target/thumbv6m-none-eabi/debug/blue_rust
```

# License


# License

The Rust code is licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)

- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

`link.x` and `nrf5x_common.ld` retain their original license.
