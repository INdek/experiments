// Copyright (C) 2017 Afonso Bordado
//
// Licensed under the MIT license <http://opensource.org/licenses/MIT>,
// This file may not be copied, modified, or distributed except according
// to the terms of that license.

#![feature(used)]
#![no_std]

#[macro_use]
extern crate ble400;

#[macro_use]
extern crate cortex_m;
#[macro_use]
extern crate cortex_m_semihosting;

use cortex_m::asm;
use ble400::ll;

mod ble;

fn main() {
    let ble;
    unsafe {
        ll::bsp_board_leds_init();
        ble = ble::Ble::new();
        ble.init_stack();
        ble.init_gap_params();
        ble.init_services();
        ble.init_advertising();
        ble.init_conn_params();

        ble400::check(ll::ble_advertising_start(
                ll::ble_adv_mode_t::BLE_ADV_MODE_FAST,
        )).unwrap();

        ble400::check(ll::app_uart_put(b'H')).unwrap();
        ble400::check(ll::app_uart_put(b'e')).unwrap();

        loop {
            ll::bsp_board_led_invert(0);

            //Power Manage
            ble400::sd_app_evt_wait().unwrap();
        }
    }
}

// As we are not using interrupts, we just register a dummy catch all handler
#[allow(dead_code)]
#[link_section = ".rodata.interrupts"]
#[used]
static INTERRUPTS: [extern "C" fn(); 240] = [default_handler; 240];

extern "C" fn default_handler() {
    asm::bkpt();
}
