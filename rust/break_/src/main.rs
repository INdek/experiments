/*
 * Copyright (C) the rdbg contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

extern crate preferences;
extern crate notify_rust;

use preferences::{AppInfo, PreferencesMap, Preferences};
use notify_rust::Notification;

use std::time::Duration;
use std::thread::sleep;

/// App info
pub const APP_INFO: AppInfo = AppInfo {
    name: "break_",
    author: "Afonso Bordado",
};

fn show_work_notification(work_time: Duration) {
    Notification::new()
        .summary("Start working")
        //TODO: we sould have better formatting
        .body(&format!("Work for {} minutes", (work_time.as_secs() / 60))[..])
        .icon("computer")
        .show().unwrap();
}

fn show_break_notification(break_time: Duration) {
    Notification::new()
        .summary("Start break")
        //TODO: we sould have better formatting
        .body(&format!("Break for {} minutes", (break_time.as_secs() / 60))[..])
        .icon("computer")
        .show().unwrap();
}

fn main() {
    let prefs_key = "config";

    // Create a new preferences key-value map
    let mut config: PreferencesMap<u64> = PreferencesMap::<u64>::load(&APP_INFO, prefs_key)
        .unwrap_or(PreferencesMap::new());

    // insert times if they don't exist yet
    let work_time = Duration::from_secs(*config.entry("work_time".to_string()).or_insert(1800));
    let break_time = Duration::from_secs(*config.entry("break_time".to_string()).or_insert(300));

    loop {
        show_work_notification(work_time);
        sleep(work_time);
        show_break_notification(break_time);
        sleep(break_time);
    }
}
